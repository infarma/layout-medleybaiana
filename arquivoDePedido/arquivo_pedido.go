package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Cabecalho    Cabecalho     `json:"Cabecalho"`
	DadosPedido  DadosPedido   `json:"DadosPedido"`
	Faturamento  Faturamento   `json:"Faturamento"`
	DadosPedido1 DadosPedido1  `json:"DadosPedido1"`
	DataPedido   DataPedido    `json:"DataPedido"`
	HoraPedido   HoraPedido    `json:"HoraPedido"`
	ItensPedido  []ItensPedido `json:"ItensPedido"`
	Rodape       Rodape        `json:"Rodape"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		var index int32
		if identificador == "1" {
			err = arquivo.Cabecalho.ComposeStruct(string(runes))
		} else if identificador == "2" {
			err = arquivo.DadosPedido.ComposeStruct(string(runes))
		} else if identificador == "3" {
			err = arquivo.Faturamento.ComposeStruct(string(runes))
		} else if identificador == "4" {
			err = arquivo.DadosPedido1.ComposeStruct(string(runes))
		} else if identificador == "5" {
			err = arquivo.DataPedido.ComposeStruct(string(runes))
		} else if identificador == "6" {
			err = arquivo.HoraPedido.ComposeStruct(string(runes))
		} else if identificador == "7" {
			err = arquivo.ItensPedido[index].ComposeStruct(string(runes))
			index++
		} else if identificador == "8" {
			err = arquivo.Rodape.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
