package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DataPedido struct {
	TipoRegistro  int32  `json:"TipoRegistro"`
	Data          int32  `json:"Data"`
	SemUtilizacao string `json:"SemUtilizacao"`
}

func (d *DataPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDataPedido

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Data, "Data")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDataPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":  {0, 1, 0},
	"Data":          {1, 9, 0},
	"SemUtilizacao": {9, 31, 0},
}
