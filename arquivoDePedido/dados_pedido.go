package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DadosPedido struct {
	TipoRegistro           int32  `json:"TipoRegistro"`
	CodigoProjeto          string `json:"CodigoProjeto"`
	Pedido                 string `json:"Pedido"`
	CnpjCentroDistribuicao string `json:"CnpjCentroDistribuicao"`
	SemUtilizacao          string `json:"SemUtilizacao"`
}

func (d *DadosPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDadosPedido

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoProjeto, "CodigoProjeto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Pedido, "Pedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CnpjCentroDistribuicao, "CnpjCentroDistribuicao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDadosPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":           {0, 1, 0},
	"CodigoProjeto":          {1, 2, 0},
	"Pedido":                 {2, 9, 0},
	"CnpjCentroDistribuicao": {9, 23, 0},
	"SemUtilizacao":          {23, 31, 0},
}
