package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DadosPedido1 struct {
	TipoRegistro  int32  `json:"TipoRegistro"`
	NumeroPedido  string `json:"NumeroPedido"`
	SemUtilizacao string `json:"SemUtilizacao"`
}

func (d *DadosPedido1) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDadosPedido1

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDadosPedido1 = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":  {0, 1, 0},
	"NumeroPedido":  {1, 16, 0},
	"SemUtilizacao": {16, 31, 0},
}
