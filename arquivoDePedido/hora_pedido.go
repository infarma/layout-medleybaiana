package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type HoraPedido struct {
	TipoRegistro  int32  `json:"TipoRegistro"`
	Hora          int32  `json:"Hora"`
	SemUtilizacao string `json:"SemUtilizacao"`
}

func (h *HoraPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHoraPedido

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.Hora, "Hora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesHoraPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":  {0, 1, 0},
	"Hora":          {1, 7, 0},
	"SemUtilizacao": {7, 31, 0},
}
