package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type ItensPedido struct {
	TipoRegistro     int32   `json:"TipoRegistro"`
	CodigoEanProduto int32   `json:"CodigoEanProduto"`
	Quantidade       int32   `json:"Quantidade"`
	TipoOcorrencia   int32   `json:"TipoOcorrencia"`
	CodigoProduto    string  `json:"CodigoProduto"`
	DescontoItem     float32 `json:"DescontoItem"`
}

func (i *ItensPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItensPedido

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoEanProduto, "CodigoEanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.TipoOcorrencia, "TipoOcorrencia")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.DescontoItem, "DescontoItem")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItensPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":     {0, 1, 0},
	"CodigoEanProduto": {1, 14, 0},
	"Quantidade":       {14, 18, 0},
	"TipoOcorrencia":   {18, 20, 0},
	"CodigoProduto":    {20, 27, 0},
	"DescontoItem":     {27, 31, 2},
}
