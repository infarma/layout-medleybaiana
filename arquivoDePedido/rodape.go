package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Rodape struct {
	TipoRegistro    int32  `json:"TipoRegistro"`
	QuantidadeItens int32  `json:"QuantidadeItens"`
	SemUtilizacao   string `json:"SemUtilizacao"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeItens, "QuantidadeItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":    {0, 1, 0},
	"QuantidadeItens": {1, 5, 0},
	"SemUtilizacao":   {5, 31, 0},
}
