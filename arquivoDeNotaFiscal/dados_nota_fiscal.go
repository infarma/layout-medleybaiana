package arquivoDeNotaFiscal

type DadosNotaFiscal struct {
	TipoRegistro          int32  `json:"TipoRegistro"`
	DataSaidaMercadoria   int32  `json:"DataSaidaMercadoria"`
	HoraSaidaMercadoria   int32  `json:"HoraSaidaMercadoria"`
	DataEmissaoNotaFiscal int32  `json:"DataEmissaoNotaFiscal"`
	CnpjLoja              string `json:"CnpjLoja"`
	NumeroNotaFiscal      int32  `json:"NumeroNotaFiscal"`
	SerieDocumento        string `json:"SerieDocumento"`
	VencimentoNotaFiscal  int32  `json:"VencimentoNotaFiscal"`
	Livre                 string `json:"Livre"`
}
