package arquivoDeNotaFiscal

type TotaisNotaFiscal struct {
	TipoRegistro         int32   `json:"TipoRegistro"`
	ValorFrete           float32 `json:"ValorFrete"`
	ValorSeguro          float32 `json:"ValorSeguro"`
	OutrasDespesas       float32 `json:"OutrasDespesas"`
	ValorTotalProdutos   float32 `json:"ValorTotalProdutos"`
	ValorTotalNotaFiscal float32 `json:"ValorTotalNotaFiscal"`
	ValorIPI             float32 `json:"ValorIPI"`
	Livre                string  `json:"Livre"`
}
