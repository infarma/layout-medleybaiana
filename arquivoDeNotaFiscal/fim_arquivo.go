package arquivoDeNotaFiscal

type FimArquivo struct {
	TipoRegistro          int32  `json:"TipoRegistro"`
	NumeroItensNotaFiscal int32  `json:"NumeroItensNotaFiscal"`
	Livre                 string `json:"Livre"`
}
