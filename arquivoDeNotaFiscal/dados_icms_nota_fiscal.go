package arquivoDeNotaFiscal

type DadosIcmsNotaFiscal struct {
	TipoRegistro                      int32   `json:"TipoRegistro"`
	BaseCalculoICMS                   float32 `json:"BaseCalculoICMS"`
	ValorICMS                         float32 `json:"ValorICMS"`
	BaseCalculoSubstituicaoTributaria float32 `json:"BaseCalculoSubstituicaoTributaria"`
	ValorICMSSbstituicaoTributaria    float32 `json:"ValorICMSSbstituicaoTributaria"`
	Livre                             string  `json:"Livre"`
}
