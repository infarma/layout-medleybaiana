package arquivoDeNotaFiscal

type Cabecalho struct {
	TipoRegistro            int32  `json:"TipoRegistro"`
	DataGeracaoArquivo      int32  `json:"DataGeracaoArquivo"`
	HoraGeracaoArquivo      int32  `json:"HoraGeracaoArquivo"`
	CnpjOperador            int32  `json:"CnpjOperador"`
	CodigoProjeto           string `json:"CodigoProjeto"`
	NumeroPedidoLaboratorio int32  `json:"NumeroPedidoLaboratorio"`
	NumeroPedidoCliente     string `json:"NumeroPedidoCliente"`
	Livre                   string `json:"Livre"`
}
