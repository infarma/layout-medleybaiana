package arquivoDeNotaFiscal

type ItensNotaFiscal struct {
	TipoRegistro           int32   `json:"TipoRegistro"`
	CodigoEan              string  `json:"CodigoEan"`
	CodigoProdutoOperador  string  `json:"CodigoProdutoOperador"`
	Quantidade             int32   `json:"Quantidade"`
	Unidade                string  `json:"Unidade"`
	PrecoFabrica           float32 `json:"PrecoFabrica"`
	DescontoComercial      float32 `json:"DescontoComercial"`
	ValorDescontoComercial float32 `json:"ValorDescontoComercial"`
	ValorRepasse           float32 `json:"ValorRepasse"`
	Repasse                float32 `json:"Repasse"`
	ValorUnitario          float32 `json:"ValorUnitario"`
	Fracionamento          float32 `json:"Fracionamento"`
	Livre                  string  `json:"Livre"`
}
