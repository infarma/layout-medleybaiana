package arquivoDeRetorno

type Cabecalho struct {
	TipoRegistro      int32  `json:"TipoRegistro"`
	CnpjCliente       string `json:"CnpjCliente"`
	CodigoProjeto     string `json:"CodigoProjeto"`
	PedidoLaboratorio string `json:"PedidoLaboratorio"`
	SemUtilizacao     string `json:"SemUtilizacao"`
}
