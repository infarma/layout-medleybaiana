package arquivoDeRetorno

type FimArquivo struct {
	TipoRegistro                   int32  `json:"TipoRegistro"`
	QuantidadeUnidadesAtendidas    int32  `json:"QuantidadeUnidadesAtendidas"`
	QuantidadeUnidadesNaoAtendidas int32  `json:"QuantidadeUnidadesNaoAtendidas"`
	QuantidadeItensArquivo         int32  `json:"QuantidadeItensArquivo"`
	SemUtilizacao                  string `json:"SemUtilizacao"`
}
