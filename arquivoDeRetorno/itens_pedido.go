package arquivoDeRetorno

type ItensPedido struct {
	TipoRegistro          int32 `json:"TipoRegistro"`
	EamProduto            int32 `json:"EamProduto"`
	QuantidadeAtendida    int32 `json:"QuantidadeAtendida"`
	QuantidadeNaoAtendida int32 `json:"QuantidadeNaoAtendida"`
	Motivo                int32 `json:"Motivo"`
	Retorno               int32 `json:"Retorno"`
}
