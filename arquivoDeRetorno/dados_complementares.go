package arquivoDeRetorno

type DadosComplementares struct {
	TipoRegistro  int32  `json:"TipoRegistro"`
	Data          int32  `json:"Data"`
	Hora          int32  `json:"Hora"`
	SemUtilizacao string `json:"SemUtilizacao"`
}
